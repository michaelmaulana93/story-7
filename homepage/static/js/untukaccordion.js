jQuery(document).ready(function($){

  var panels = $('.accordion > dd');
  panels.hide();


  // script ketika diclick
  $('.accordion > dt > a').click(function() {

    var $this = $(this);
    // Slide up ketika diclick salah satu diclick
    panels.slideUp();

    // Slide down panel yang diclick
    $this.parent().next().slideDown();

    return false;
  });
});